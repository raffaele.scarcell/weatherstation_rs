#pragma once

#include "Logger.h"
#include "GAPPeripheral.h"
#include "NRFDevKit.h"
#include "IDevKit.h"
#include "LPS25HBDevice.h"
#include "HDC1000Device.h"
#include "MesurementHistory.h"
#include "BatteryService.h"
#include "EnvironmentalService.h"

class WeatherStation :  public GAPPeripheral
{
public:
  // constructor
  WeatherStation(BLE& ble, IDevKit& devKit, Logger& logger); //IDevKit& devKit
  
  // called for starting the weather station
  // it will start getting measurements and advertise with data measurements
  void start();
  
  // called by timer
  void performMeasurements(void);

  //
  void setupAdvertisementPayload(double temperature, double humidity, double pressure, uint8_t batteryLevel);


    
private:
  // data members
  // instance of sensor devices
  LPS25HBDevice m_lps25hbdevice;
  HDC1000Device m_hdc1000device;
    
  // event queue for handling all events (including timers)
  events::EventQueue m_eventQueue;

  //values stored using istance of MesurementHistory
  MesurementHistory m_mesurementHistory;
    
  // measurement interval
  static const int m_measurementInterval=30000; //30 sec 
  
  // blink interval
  static const int m_blinkInterval=1000; //1 sec for blink
  
  // reference to dev kit
  IDevKit& m_devKit;
  
  // reference to logger
  Logger& m_logger;

  // buffer for service data
  static const int SERVICE_DATA_LENGTH = 8;
  uint8_t m_serviceDataBuffer[SERVICE_DATA_LENGTH];

  //services
  BatteryService m_batteryService;
  EnvironmentalService m_environmentalService;

  //redefine the onInitComplete() 
  virtual void onInitComplete(BLE::InitializationCompleteCallbackContext *event);

};