#include "HDC1000Device.h"

#include "mbed_error.h"

// definitions
#define HDC1000_TEMPERATURE                0x00
#define HDC1000_HUMIDITY                   0x01

HDC1000Device::HDC1000Device(PinName sda, PinName scl, PinName dataRdy, Logger& logger)
: I2C(sda, scl),
  m_address(0x40 << 1),
  m_dataReadyPin(dataRdy, PullUp),
  m_logger(logger) {
   
}

bool HDC1000Device::probe(void) {
  uint8_t rx_data = 0;
  int rc = read(m_address | 0x1, (char*) &rx_data, (int) sizeof(rx_data));
  return (rc == 0);
}

double HDC1000Device::getTemperature(void) {
  double temperature = getRawTemperature();
  return (temperature / 65536.0) * 165.0 - 40.0;
}


double HDC1000Device::getHumidity(void) {
  double humidity = getRawHumidity();
  return (humidity / 65536.0) * 100.0;
}

uint16_t HDC1000Device::getRawTemperature(void) {
  mbed_error_status_t rc = setReadRegister(HDC1000_TEMPERATURE);
  if (rc != MBED_SUCCESS) {
    MBED_ERROR( MBED_MAKE_ERROR(MBED_MODULE_APPLICATION, rc), "HDC1000Device: getRawTemperature() failed");
  }
  return read16();
}

uint16_t HDC1000Device::getRawHumidity(void) {
  mbed_error_status_t rc = setReadRegister(HDC1000_HUMIDITY);
  if (rc != MBED_SUCCESS) {
    MBED_ERROR( MBED_MAKE_ERROR(MBED_MODULE_APPLICATION, rc), "HDC1000Device: getRawHumidity() failed");
  }
  return read16();
}

mbed_error_status_t  HDC1000Device::setReadRegister(uint8_t reg) {
 
  // write register, device will thus read data and signal data ready
  const char txData[1] = { reg };  
  int rc = write(m_address, txData, (int) sizeof(txData), true);
  if (rc != 0) {
    MBED_ERROR(MBED_MAKE_ERROR(MBED_MODULE_APPLICATION, MBED_ERROR_CODE_WRITE_FAILED), "HDC1000Device: Could not write to register");    
  }

  // busy wait for data ready
  uint16_t iteration = 0;
  while (m_dataReadyPin == 1 || iteration++ < 100) {
    ThisThread::sleep_for(10);
  }

  return MBED_SUCCESS;
}

uint16_t HDC1000Device::read16(void)
{
  char rxData[2] = { 0 };
  int rc = read(m_address, rxData, sizeof(rxData));
  if (rc != 0) {
    MBED_ERROR( MBED_MAKE_ERROR(MBED_MODULE_APPLICATION, MBED_ERROR_CODE_READ_FAILED), "HDC1000Device: Could not read data");
  }
  
  uint16_t value = (((uint16_t) rxData[0]) << 8) + (uint16_t) rxData[1];
  //m_logger.log("Value read is %d (%d %d)\r\n", value, rxData[1], rxData[0]);

  return value;
}