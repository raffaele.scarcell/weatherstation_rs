#pragma once

#include <mbed.h>

class Logger {
public:
  // constructor
  Logger(bool logTimestamp);
  
  // method called for printing a message, preprended with timestamp information if requested
  void log(const char * format, ...);
  
private:
  bool m_logTimestamp;
  Timer t;
};