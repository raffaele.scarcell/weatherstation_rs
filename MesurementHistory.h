#pragma once

#include <stdint.h>
#include <time.h>
#include "EnvironmentalService.h"

class MesurementHistory{

public:
    MesurementHistory();

    //get size
    int getSize(void) const;

    //add mesurement
    void addMesurement(double temperature, double humidity, double pressure);

    //compute the mean value of the barometric pressure
    double getMeanPressure(void) const;
    
    EnvironmentalService::PressureTrendType_t getPressureTrend(double) const;
private:
    struct Mesurement {
        time_t m_time;
        int16_t m_temperature;
        uint8_t m_humidity;
        uint16_t m_pressure;
    };

    int m_index;
    static const int MEMORY_SIZE = 1024;
    static const int N = MEMORY_SIZE / sizeof(Mesurement);
    Mesurement m_mesurementArray[N];
};


