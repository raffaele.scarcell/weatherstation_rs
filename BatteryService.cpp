#include "BatteryService.h"

#include "mbed.h"

BatteryService::BatteryService(BLE& ble, Logger& logger)
: m_ble(ble),
  m_serviceAdded(false),
  m_batteryLevelCharacteristic(GattCharacteristic::UUID_BATTERY_LEVEL_CHAR, &m_batteryLevel),
  m_logger(logger) {
}

void BatteryService::addServiceToGattServer(void) {
  // We should only ever need to add the information service once
  if (m_serviceAdded) {
    return;
  }

  GattCharacteristic *charTable[] = { &m_batteryLevelCharacteristic };
                                      
  GattService batteryService(GattService::UUID_BATTERY_SERVICE, charTable, sizeof(charTable) / sizeof(GattCharacteristic *));
  m_ble.gattServer().addService(batteryService);
  
  m_serviceAdded = true;
  
  m_logger.log("Battery service added\r\n");
}

 void BatteryService::updateBatteryLevel(BatteryLevelType_t newBatteryLevelVal) {
  m_batteryLevel = newBatteryLevelVal;
  m_ble.gattServer().write(m_batteryLevelCharacteristic.getValueHandle(), (uint8_t *) &m_batteryLevel, sizeof(BatteryLevelType_t));
}
