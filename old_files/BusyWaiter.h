#pragma once

#include <mbed.h>

class BusyWaiter {
public:
  // constructor
  BusyWaiter();
  
  // method called for busy waiting
  static void wait(int ms);
};
