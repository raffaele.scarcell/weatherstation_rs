#pragma once

#include <mbed.h>
#include "IDevkit.h"
#include "Logger.h"

class NRFDevKit : public IDevKit {
public:
  // constructor
  NRFDevKit(Logger& logger);

  //IDevKit implementation
  virtual void toggleLed1(void);
  virtual uint8_t getBatteryLevel(void);
  virtual PinName getSCL(void);
  virtual PinName getSDA(void);
  virtual PinName getDataRdy(void);
  
private:
  // data members
  // array of leds
  DigitalOut m_led1;
  
  // logger instance
  Logger& m_logger;
};