#pragma once

#include "ble/BLE.h"

// for logging
#include "Logger.h"

class BatteryService {
public:
  typedef int8_t BatteryLevelType_t;
  
  /**
   * @brief   BatteryService constructor.
   * @param   ble Reference to BLE device.
   */
  BatteryService(BLE& ble, Logger& logger);
  
  /**
   * Called after initialization of the BLE module for adding the service
   * to the GATT server
   */
  void addServiceToGattServer(void);
   
  /**
   * @brief   Update battery level characteristic.
   * @param   newBatteryLevelVal New battery level measurement.
   */
  void updateBatteryLevel(BatteryLevelType_t newBatteryLevelVal);

private:
  // data members
  BLE& m_ble;
  bool m_serviceAdded;

  BatteryLevelType_t m_batteryLevel;
  
  // characteristics belonging to the service
  ReadOnlyGattCharacteristic<BatteryLevelType_t> m_batteryLevelCharacteristic;
  // logger instance
  Logger& m_logger;
};