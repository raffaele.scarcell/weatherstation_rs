#pragma once

// mbed os
#include <mbed.h>
#include "ble/BLE.h"

// stl
#include <string>
#include <list>

// local
#include "IDevKit.h"
#include "Logger.h"

class GAPPeripheral : 
  private mbed::NonCopyable<GAPPeripheral>, 
  public ble::Gap::EventHandler {
public:
  // constructor
  GAPPeripheral(BLE& ble, 
                IDevKit& devKit, 
                const string& deviceName,
#ifdef _BLE_MBED_511  
                ble::advertising_type_t advType,
#else
                GapAdvertisingParams::AdvertisingType_t advType,
#endif
                events::EventQueue& eventQueue,
                Logger& logger);
  
  // destructor
  ~GAPPeripheral();

#if defined(_BLE_MBED_511)
  // called for starting advertising
  void advertise(UUID serviceUUIDArray[], size_t arraySize);
#else
  // called for starting advertising
  void advertise(void);
#endif

protected:
  // protected methods (can be redefined by inheriting classes)
  
  // This is called when BLE interface is initialised
  virtual void onInitComplete(BLE::InitializationCompleteCallbackContext *event);
  
  // methods used for setting the advertising data payload for a specific service
#if defined(_BLE_MBED_511)
  void setServiceData(UUID serviceUUID, uint8_t* pServiceData, uint16_t serviceDataLength);
#else
static const int SERVICE_DATA_LENGTH = 8;
  struct ServiceDataPayload {
    uint16_t serviceUUID;
    uint8_t serviceData[SERVICE_DATA_LENGTH];
    uint16_t serviceDataLength;
  };
  void setAdvertisementServiceData(ServiceDataPayload* serviceDataPayloadArray,
                                   uint8_t nbrOfServices);
#endif  
  static void uint16_encode(const uint16_t value, uint8_t* p_encoded_data);
  static void int16_encode(const int16_t value, uint8_t* p_encoded_data);
  static void uint32_encode(const uint32_t value, uint8_t* p_encoded_data);

private:
  // private methods

#if defined(_BLE_MBED_511)
  // methods used for setting the list of service uuids in the advertising packet
  ble_error_t setLocalServiceList(UUID* serviceUUIDArray, uint8_t arraySize);
#endif
  
  // Schedule processing of events from the BLE middleware in the event queue. */
  void scheduleBleEvents(BLE::OnEventsToProcessCallbackContext *context);
  
  // called if timeout is reached during advertising, scanning or connection initiation
  void onTimeOut(const Gap::TimeoutSource_t source);
  
  // This is called when BLE interface receives a connection request
  void onConnect(const Gap::ConnectionCallbackParams_t *connection_event);
  
  // Set up and start advertising
  void startAdvertising(void);
  
protected:
  // protected data members
  IDevKit& m_devKit;
  
private:
  // private data members
  BLE& m_ble;
  const string& m_deviceName;
#ifdef _BLE_MBED_511  
  ble::advertising_type_t m_advType;
#else
  GapAdvertisingParams::AdvertisingType_t m_advType;
#endif
  events::EventQueue& m_eventQueue;
  
  // blink interval
  static const int BLINK_INTERVAL = 1000;
  
  // advertising interval
  static const int ADVERTISING_INTERVAL = 200; 

#ifdef _BLE_MBED_511  
  // buffer and builder used for advertising payload
  uint16_t m_advertisingBufferSize;
  uint8_t* m_pAdvertisingBuffer; 
  ble::AdvertisingDataBuilder* m_pAdvertisingDataBuilder;
#endif
  // logger instance
  Logger& m_logger;
};