#pragma once

#include "mbed.h"

class IDevKit {
public:  
  // method called for toggling led 1
  virtual void toggleLed1(void) = 0;
  
  // method called for getting battery level
  virtual uint8_t getBatteryLevel(void) = 0;
  
  // for configuring the I2C bus
  virtual PinName getSCL(void) = 0;
  virtual PinName getSDA(void) = 0;
  virtual PinName getDataRdy(void) = 0;
};
  