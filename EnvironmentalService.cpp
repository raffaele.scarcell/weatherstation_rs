#include "EnvironmentalService.h"

#include "mbed.h"

EnvironmentalService::EnvironmentalService(BLE& ble, Logger& logger)
: m_ble(ble),
  m_serviceAdded(false),
  m_humidityCharacteristic(GattCharacteristic::UUID_HUMIDITY_CHAR,&m_humidity),
  m_pressureCharacteristic(GattCharacteristic::UUID_PRESSURE_CHAR,&m_pressure),
  m_temperatureCharacteristic(GattCharacteristic::UUID_TEMPERATURE_CHAR,&m_temperature),
  m_pressureTrendCharacteristic(EnvironmentalService::UUID_PRESSURE_TREND_CHAR,&m_pressureTrend),
  m_logger(logger){

}

void EnvironmentalService::addServiceToGattServer(void){
    // We should only ever need to add the information service once
  if (m_serviceAdded) {
    return;
  }

  GattCharacteristic *charTable[] = { &m_humidityCharacteristic,&m_pressureCharacteristic,&m_temperatureCharacteristic,&m_pressureTrendCharacteristic };
                                      
  GattService environmentalService(GattService::UUID_ENVIRONMENTAL_SERVICE, charTable, sizeof(charTable) / sizeof(GattCharacteristic *));
  m_ble.gattServer().addService(environmentalService);
  
  m_serviceAdded = true;
  
  m_logger.log("Environmental service added\r\n");

}


void EnvironmentalService::updateHumidity(HumidityType_t newHumidityVal){
    m_humidity = newHumidityVal;
    m_ble.gattServer().write(m_humidityCharacteristic.getValueHandle(), (uint8_t *) &m_humidity, sizeof(HumidityType_t));
}

void EnvironmentalService::updatePressure(PressureType_t newPressureVal){
    m_pressure = newPressureVal;
    m_ble.gattServer().write(m_pressureCharacteristic.getValueHandle(), (uint8_t *) &m_pressure, sizeof(PressureType_t));
}

void EnvironmentalService::updateTemperature(TemperatureType_t newTemperatureVal){
    m_temperature = newTemperatureVal;
    m_ble.gattServer().write(m_temperatureCharacteristic.getValueHandle(), (uint8_t *) &m_temperature, sizeof(TemperatureType_t));
}

void EnvironmentalService::updateBarometricPressureTrend(PressureTrendType_t newPressureTrendVal){
    m_pressureTrend = newPressureTrendVal;
    m_ble.gattServer().write(m_pressureTrendCharacteristic.getValueHandle(), (uint8_t *) &m_pressureTrend, sizeof(PressureTrendType_t));
}