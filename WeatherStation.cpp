#include "WeatherStation.h"
#include "GAPPeripheral.h"
#define MESUREMENT_INTERVAL 20000


// Device name
static const string g_deviceName = "WS";

// constructor
WeatherStation::WeatherStation(BLE& ble, IDevKit& devKit, Logger& logger) 
#if defined(_BLE_MBED_511)
//DA CAMBIARE TYPE
//: GAPPeripheral(ble, devKit, g_deviceName, ble::advertising_type_t::NON_CONNECTABLE_UNDIRECTED, m_eventQueue, logger),
: GAPPeripheral(ble, devKit, g_deviceName, ble::advertising_type_t::ADV_IND, m_eventQueue, logger),
#else 
//: GAPPeripheral(ble, devKit, g_deviceName, GapAdvertisingParams::ADV_NON_CONNECTABLE_UNDIRECTED, m_eventQueue, logger),
: GAPPeripheral(ble, devKit, g_deviceName, GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED, m_eventQueue, logger),
#endif
 // newly declared devices must be initialized here
  m_lps25hbdevice(devKit.getSDA(), devKit.getSCL(), logger),
  m_hdc1000device(devKit.getSDA(), devKit.getSCL(), devKit.getDataRdy(),logger),
  m_devKit(devKit),
  m_batteryService(ble,logger),
  m_environmentalService(ble, logger),
  m_logger(logger) {
}

void WeatherStation::start() {
  m_logger.log("WeatherStation is starting\r\n");
  m_logger.log("%d Data will be stored\r\n", m_mesurementHistory.getSize());
  // make sure that the LPW25HB device is present
  // if not log an error and return
  if(!m_lps25hbdevice.probe()){
      m_logger.log("LPS25HB is not present");
      return;
  }
  
  m_logger.log("LPS25HB device found\r\n");    
  
  // make sure that the HDC1000 device is present
  // if not log an error and return
  if(!m_lps25hbdevice.probe()){
      m_logger.log("HDC1000 is not present");
      return;
  }
  
  m_logger.log("HDC1000 device found\r\n");
  

    // Event for measurements
    EventQueue mesurementQueue(32 * EVENTS_EVENT_SIZE);
    mesurementQueue.call_every(MESUREMENT_INTERVAL, this, &WeatherStation::performMeasurements);
    Thread t;
    t.start(callback(&mesurementQueue, &EventQueue::dispatch_forever));
    
    
    //Start advertising
    GAPPeripheral::advertise();
}
 
void WeatherStation::performMeasurements(void) {
  m_logger.log("\r\n ****** Performing measurements ****** \r\n");

  // get and log pressure LPS
  double pressure = m_lps25hbdevice.getPressure();
  m_logger.log("Pressure: %f \r\n", pressure);
  m_environmentalService.updatePressure(pressure*10);


  // get and log temperature HDC
  double temperature = m_hdc1000device.getTemperature();
  m_logger.log("Temperature: %f \r\n", temperature);
  m_environmentalService.updateTemperature(temperature*100);


  // get and log humidity HDC
  double humidity = m_hdc1000device.getHumidity();
  m_logger.log("Humidity: %f \r\n", humidity);
  m_environmentalService.updateHumidity(humidity*100);


  // get and log the battery level DEVKIT
  uint8_t battery = m_devKit.getBatteryLevel();
  m_logger.log("Battery level: %f \r\n \r\n", battery);
  m_batteryService.updateBatteryLevel(battery);


  EnvironmentalService::PressureTrendType_t newTrend = m_mesurementHistory.getPressureTrend(pressure);
  m_environmentalService.updateBarometricPressureTrend(newTrend);
  m_logger.log("Barometric pressur trend: %f \r\n",newTrend);

  //Stock mesurements 
  m_mesurementHistory.addMesurement(temperature, humidity, pressure);

  //Setup advertising data
  setupAdvertisementPayload(temperature, humidity, pressure, battery); 

  

}

void WeatherStation::setupAdvertisementPayload(double temperature, double humidity, double pressure, uint8_t battery){

    // Add service data for battery 
    uint8_t serviceDataLength = 0;
    memset(m_serviceDataBuffer,0,sizeof(*m_serviceDataBuffer));
    m_serviceDataBuffer[0] = battery;
    serviceDataLength = 1;
 
    ServiceDataPayload serviceDataPayload[1] = {0};
    serviceDataPayload[0].serviceUUID = GattService::UUID_ENVIRONMENTAL_SERVICE;
    
    //encode pressure as an unsigned integer of 32 bits (with 1 decimal)
    uint32_encode((uint32_t)(pressure*10),&serviceDataPayload[0].serviceData[serviceDataPayload[0].serviceDataLength]);
    serviceDataPayload[0].serviceDataLength += 4;
 
    //encode temperature as a signed integer of 16 bits (with 2 decimals)
    int16_encode((int16_t)(temperature*100), &serviceDataPayload[0].serviceData[serviceDataPayload[0].serviceDataLength]);
    serviceDataPayload[0].serviceDataLength += 2;
 
    //encode temperature as a signed integer of 16 bits (with 2 decimals)
    uint16_encode((uint16_t)(humidity*100), &serviceDataPayload[0].serviceData[serviceDataPayload[0].serviceDataLength]);
    serviceDataPayload[0].serviceDataLength += 2;
 
    GAPPeripheral::setAdvertisementServiceData(serviceDataPayload,serviceDataLength);

}



  //redefine the onInitComplete() 
void WeatherStation::onInitComplete(BLE::InitializationCompleteCallbackContext *event){
    GAPPeripheral::onInitComplete(event);

    m_batteryService.addServiceToGattServer();
    m_environmentalService.addServiceToGattServer();

}