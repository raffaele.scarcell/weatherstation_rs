#include "MesurementHistory.h"

#include <stdint.h>
#include <string.h>

MesurementHistory::MesurementHistory()
: m_index(0) {
    memset(&m_mesurementArray, 0, sizeof(m_mesurementArray));
}

int MesurementHistory::getSize(void) const{
    return N;
}

void MesurementHistory::addMesurement(double temperature, double humidity, double pressure){
    m_mesurementArray[m_index % N].m_time = time(NULL);
    m_mesurementArray[m_index % N].m_temperature = (uint16_t) (temperature * 100);
    m_mesurementArray[m_index % N].m_humidity = (uint8_t) (humidity);
    m_mesurementArray[m_index % N].m_pressure = (uint16_t) (pressure * 10);

    m_index++;
}

double MesurementHistory::getMeanPressure(void) const {
    if(m_index <= 0){
        return 0.0;
    }

    int size = N;
    if (m_index < N){
        size = m_index;
    }

    double mean = 0.0;
    for(int i = 0; i<size; i++){
        double pressure = ((double) m_mesurementArray[i].m_pressure) /10.0;
        mean += pressure;
    }

    mean=mean/size;
    
    return mean;
}

EnvironmentalService::PressureTrendType_t MesurementHistory::getPressureTrend(double newPressure) const {
    double mean = getMeanPressure();

    EnvironmentalService::PressureTrendType_t trend = EnvironmentalService::Unknown;

    if(mean > 0.0) {
        if(newPressure > mean){
            trend = EnvironmentalService::ContinuouslyRising;
        }
        else {
            trend = EnvironmentalService::ContinuouslyFalling;
        }
    }

    return trend;
}