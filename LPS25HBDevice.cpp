#include "LPS25HBDevice.h"

#include "mbed_error.h"

// registers
// Who am I register location
#define LPS25HB_WHO_AM_I              0x0F
// Tells whether the Pressure Data is ready or is being overrun
#define LPS25HB_STATUS_REG            0x27
// (LSB) Pressure output value
#define LPS25HB_PRESS_OUT_XL          0x28
// (mid part) Pressure output value
#define LPS25HB_PRESS_OUT_L           0x29
// (MSB) Pressure output value
#define LPS25HB_PRESS_OUT_H           0x2A
// (LSB) Pressure output value
#define LPS25HB_TEMP_OUT_L            0x2B
// (mid part) Pressure output value
#define LPS25HB_TEMP_OUT_H            0x2C
// Contains PD, BDU and more
#define LPS25HB_CTRL_REG1             0x20
// Contains one-shot mode and FIFO settings
#define LPS25HB_CTRL_REG2             0x21
// Pressure and temperature Resolution
#define LPS25HB_RES_CONF              0x10

// Configuration Bits
// Power Down when 0, active mode when 1 (Default 0)
#define LPS25HB_CTRL_REG1_PD          0x80
// Block Data Update: 0 Continuous mode, 1 read LSB,Mid,MSB first
#define LPS25HB_CTRL_REG1_BDU         0x4
// One shot mode enabled, obtains a new dataset
#define LPS25HB_CTRL_REG2_ONE_SHOT  0x1
//Pressure data available
#define LPS25HB_STATUS_REG_PDA      0x2

LPS25HBDevice::LPS25HBDevice(PinName sda, PinName scl, Logger& logger)
: I2C(sda, scl),
  m_logger(logger),
  m_address(0x5D << 1) {
   
}

bool LPS25HBDevice::probe(void) {
  uint8_t rx_data = 0;
  int rc = read(m_address | 0x1, (char*) &rx_data, (int) sizeof(rx_data));
  return (rc == 0);
}
  
double LPS25HBDevice::getPressure(void) {
  // One-shot mode measurement sequence
  // 1. Power down the device (clean start)
  // WriteByte(LPS25HB_CTRL_REG1 = 0x00); // @0x20=0x00
  int rc = writeByte(LPS25HB_CTRL_REG1, 0);
  if (rc != 0) {
    m_logger.log("Error\r\n");
    return 0.0;
  }
  
  // 2. Turn on the pressure sensor analog front end in single shot mode
  // WriteByte(LPS25HB_CTRL_REG1 = 0x84); // @0x20=0x84
  rc = writeByte(LPS25HB_CTRL_REG1, LPS25HB_CTRL_REG1_PD | LPS25HB_CTRL_REG1_BDU);
  if (rc != 0) {
    m_logger.log("Error\r\n");
    return 0.0;
  }

  // 3a. Run one-shot measurement (temperature and pressure), the set bit will be reset by the
  //  sensor itself after execution (self-clearing bit)
  //  WriteByte(LPS25HB_CTRL_REG2 = 0x01); // @0x21=0x01
  rc = writeByte(LPS25HB_CTRL_REG2, LPS25HB_CTRL_REG2_ONE_SHOT);
  if (rc != 0) {
    m_logger.log("Error\r\n");
    return 0.0;
  }

  // 3b. Configure the resolution for pressure for 16 internal averages
  //  WriteByte(LPS25HB_RES_CONF = 0x01); // @0x10=0x01
  rc = writeByte(LPS25HB_RES_CONF, LPS25HB_CTRL_REG2_ONE_SHOT);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  // 4. Wait until the measurement is completed
  // ReadByte(CTRL_REG2_ADDR = 0x00); // @0x21=0x00
  uint8_t status = 0;
  do {
    rc = readByte(LPS25HB_CTRL_REG2, status);
    if (rc != 0)
    {
      m_logger.log("Error\r\n");
    }    
  } while (status != 0x00);

//  // Wait for Temperature data to be ready
//  do {
//    err_code = lps25hb_read_byte(LPS25HB_STATUS_REG, &status);
//    APP_ERROR_CHECK(err_code);
//  } while ((status & LPS25HB_STATUS_REG_PDA) == 0);

  //  5. Read the temperature measurement (2 bytes to read)
  //  Read((u8*)pu8, TEMP_OUT_ADDR, 2); // @0x2B(OUT_L)~0x2C(OUT_H)
  //  Temp_Reg_s16 = ((u16) pu8[1]<<8) | pu8[0]; // make a SIGNED 16 bit variable
  //  Temperature_DegC = 42.5 + Temp_Reg_s16 / 480; // offset and scale
  uint8_t temp_L = 0;
  rc = readByte(LPS25HB_TEMP_OUT_L, temp_L);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  uint8_t temp_H = 0;
  rc = readByte(LPS25HB_TEMP_OUT_H, temp_H);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  // int16_t temp16 = ((uint16_t) temp_H << 8) | temp_L;
  // double tempC = 42.5 + (((double) temp16) / 480);
  // int temperature_int = (int) tempC;
  //int temperature_frac = 0;
  //if (tempC > 0.0) {
  //  temperature_frac = ((int) ((tempC - temperature_int) * 1000));
  //}
  //else {
  //  temperature_frac = ((int) ((-tempC + temperature_int) * 1000));
  //}
  
  //  6. Read the pressure measurement
  //  Read((u8*)pu8, PRESS_OUT_ADDR, 3);
  //  @0x28(OUT_XL)~0x29(OUT_L)~0x2A(OUT_H)
  //  Pressure_Reg_s32 = ((u32)pu8[2]<<16)|((u32)pu8[1]<<8)|pu8[0];
  //  make a SIGNED 32 bit Pressure_mb = Pressure_Reg_s32 / 4096; // scale
  uint8_t pressure_XL = 0;
  rc = readByte(LPS25HB_PRESS_OUT_XL, pressure_XL);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  uint8_t pressure_L = 0;
  rc = readByte(LPS25HB_PRESS_OUT_L, pressure_L);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  uint8_t pressure_H = 0;
  rc = readByte(LPS25HB_PRESS_OUT_H, pressure_H);
  if (rc != 0) {
    m_logger.log("Error\r\n");
  }

  int32_t pressure32 = ((pressure_H << 16) | (pressure_L << 8) | (pressure_XL));

  // convert the 2's complement 24 bit to 2's complement 32 bit
  if (pressure32 & 0x00800000) {
    pressure32 |= 0xFF000000;
  }

  // Calculate Pressure in mbar or hPa
  double pressure = ((double) pressure32) / 4096.0f;

  //  7. Check the temperature and pressure values make sense
  // Reading fixed 760 hPa, means the sensing element is damaged.
  if (pressure == 760) {
    return 0.0;
  }

  return pressure;
}

int LPS25HBDevice::writeByte(uint8_t regAddress, uint8_t value) {
  uint8_t txData[2] = { regAddress, value };

  int rc = write(m_address, (char*) txData, sizeof(txData));
  return rc;  
}
  
int LPS25HBDevice::readByte(uint8_t regAddress, uint8_t& value) {
  // first write register
  uint8_t txData[1] = { regAddress };
  int rc = write(m_address, (char*) txData, sizeof(txData), true);
  if (rc != 0) {
    m_logger.log("Error in read %d\r\n", rc);
    return rc;
  }
  rc = read(m_address, (char*) &value, sizeof(value));
  
  return rc;
}
