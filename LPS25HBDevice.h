#pragma once

#include "mbed.h"

#include "Logger.h"

class LPS25HBDevice : public I2C {
public:
  // constructor
  LPS25HBDevice(PinName sda, PinName scl, Logger& logger);   
  
  // method for checking device presence
  bool probe(void);
  
  // method for getting the measured pressure
  double getPressure(void);
  
private:
  // private methods
  int writeByte(uint8_t regAddress, uint8_t value);
  int readByte(uint8_t regAddress, uint8_t& value);
  
  // data members
  int m_address;
  
  // logger instance
  Logger& m_logger;
};