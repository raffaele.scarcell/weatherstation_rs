#pragma once

#include "mbed.h"

#include "Logger.h"

class HDC1000Device : public I2C {
public:
  // constructor
  HDC1000Device(PinName sda, PinName scl, PinName dataRdy, Logger& logger);
    
  // method for checking device presence
  bool probe(void);
  
  // method for getting the different measurements
  double getTemperature(void);
  double getHumidity(void);
  
private:
  // private methods
  uint16_t getRawTemperature(void);
  uint16_t getRawHumidity(void);
  mbed_error_status_t setReadRegister(uint8_t reg);
  uint16_t read16(void);
  
private:
  // data members
  
  int m_address;
  DigitalIn m_dataReadyPin; 
  
  // logger instance
  Logger& m_logger;
};