#pragma once

#include "ble/BLE.h"

/**
 * Read, Write, Notify, Indicate  Characteristic declaration helper.
 *
 * @tparam T type of data held by the characteristic.
 */
 template<typename T>
 class ReadNotifyGattCharacteristic : public GattCharacteristic {
 public:
   /**
    * Construct a characteristic that can be read or written and emit
    * notification or indication.
    *
    * @param[in] uuid The UUID of the characteristic.
    * @param[in] initial_value Initial value contained by the characteristic.
   */
   ReadNotifyGattCharacteristic(const UUID & uuid, T* initial_value)
   : GattCharacteristic(
                /* UUID */ uuid,
                /* Initial value */ reinterpret_cast<uint8_t*>(initial_value),
                /* Value size */ sizeof(T),
                /* Value capacity */ sizeof(T),
                /* Properties */ GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
                                 GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY,
                /* Descriptors */ NULL,
                /* Num descriptors */ 0,
                /* variable len */ false) {
        
   }
};
