/* mbed Microcontroller Library
 * Copyright (c) 2019 ARM Limited
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbed.h"
#include "platform/mbed_thread.h"
//#include "BusyWaiter.h" //removed
#include "Logger.h"
#include "NRFDevKit.h"
#include "IDevKit.h"
#include "WeatherStation.h"
#include "GAPPeripheral.h"

// Blinking rate in milliseconds
//#define BLINKING_RATE_MS    500

int main()
{   
    //logger init
    Logger logger(true);

    //initialization devkit & WeatherStation
    NRFDevKit devkit(logger);
    WeatherStation weatherStation(BLE::Instance(), devkit, logger);

    weatherStation.start();
}