#include "Logger.h"

Logger::Logger(bool logTimestamp) :
m_logTimestamp(logTimestamp) {
    t.start();
}

void Logger::log(const char* format, ...) {
  va_list paramList;
  va_start(paramList, format);
  
  // TP 2: implement the logging of timestamp here
  printf("\r\n Time: %f : ",t.read());
  vprintf(format, paramList);
}