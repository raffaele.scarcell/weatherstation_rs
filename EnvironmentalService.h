#pragma once

#include "ble/BLE.h"
#include "ReadNotifyGattCharacteristic.h"

// for logging
#include "Logger.h"

class EnvironmentalService {
public:

    typedef uint32_t PressureType_t;
    typedef int16_t TemperatureType_t;
    typedef uint16_t HumidityType_t;
    
    enum {
        UUID_PRESSURE_TREND_CHAR = 0x2AA3
    };
    enum PressureTrendType_t{
        Unknown = 0,
        ContinuouslyFalling = 1,
        ContinuouslyRising = 2,
        FallingThenSteady = 3,
        RisingThenSteady = 4,
        FallingBeforeALesserRise = 5,
        FallingBeforeAGreaterRise = 6,
        RisingBeforeAGreaterFall = 7,
        RisingBeforeALesserFall = 8,
        Steady = 9                                                  
    };



    /**
    * @brief   EnvironmentalService constructor.
    * @param   ble Reference to BLE device.
    */
    //BatteryService(BLE& ble, Logger& logger);
    EnvironmentalService(BLE& ble, Logger& logger);

    /**
    * Called after initialization of the BLE module for adding the service
    * to the GATT server
    */
    void addServiceToGattServer(void);

    /**
    * @brief   Update pressure level characteristic.
    * @param   newPressureVal New pressure level measurement.
    */
    void updatePressure(PressureType_t newPressureVal);

    /**
    * @brief   Update temperature level characteristic.
    * @param   newTemperatureVal New temperature level measurement.
    */
    void updateTemperature(TemperatureType_t newTemperatureVal);

    /**
    * @brief   Update humidity level characteristic.
    * @param   newHumidityVal New humidity level measurement.
    */
    void updateHumidity(HumidityType_t newHumidityVal);

    /**
    * @brief   Update pressure trend level characteristic.
    * @param   newPressureTrendVal New pressure trend level measurement.
    */
    void updateBarometricPressureTrend(PressureTrendType_t newPressureTrendVal);


private:
    // data members
    BLE& m_ble;
    bool m_serviceAdded;

    PressureType_t m_pressure;
    TemperatureType_t m_temperature;
    HumidityType_t m_humidity;
    PressureTrendType_t m_pressureTrend;

    // characteristics belonging to the service
    ReadNotifyGattCharacteristic<PressureType_t> m_pressureCharacteristic;
    ReadNotifyGattCharacteristic<TemperatureType_t> m_temperatureCharacteristic;
    ReadNotifyGattCharacteristic<HumidityType_t> m_humidityCharacteristic;
    ReadNotifyGattCharacteristic<PressureTrendType_t> m_pressureTrendCharacteristic;
    
    // logger instance
    Logger& m_logger;
};